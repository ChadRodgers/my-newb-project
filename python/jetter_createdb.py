# Import sqlite3 and create global variables for connection and cursor
import sqlite3

conn = sqlite3.connect('jetter.db')
c = conn.cursor()
conn.execute("PRAGMA foreign_keys = ON")

# Create tables
def CreateWorkorder():
    c.execute('''CREATE TABLE IF NOT EXISTS workorder (id INTEGER PRIMARY KEY AUTOINCREMENT, 
    ord INTEGER UNIQUE
    )''')

def CreateWorkorderToSaleorder():
    c.execute('''CREATE TABLE IF NOT EXISTS wo_to_so (wo_id INTEGER, 
    so_id INTEGER,
    FOREIGN KEY(wo_id) REFERENCES workorder(id),
    FOREIGN KEY(so_id) REFERENCES salesorder(id)
    )''')

def CreateSalesorder():
    c.execute('''CREATE TABLE IF NOT EXISTS salesorder (id INTEGER PRIMARY KEY AUTOINCREMENT, 
    ord TEXT UNIQUE, 
    desc TEXT, 
    note TEXT
    )''')

def CreateSalesorderToSchematic():
    c.execute('''CREATE TABLE IF NOT EXISTS so_to_sch (so_id INTEGER, 
    sch_id INTEGER,
    FOREIGN KEY(so_id) REFERENCES salesorder(id),
    FOREIGN KEY(sch_id) REFERENCES schematic(id)
    )''')

def CreateSchematic():
    c.execute('''CREATE TABLE IF NOT EXISTS schematic (id INTEGER PRIMARY KEY AUTOINCREMENT, 
    item TEXT UNIQUE, 
    desc TEXT, 
    type TEXT, 
    note TEXT
    )''')

if __name__ == "__main__":
        CreateWorkorder()
        CreateSalesorder()
        CreateSchematic()
        CreateWorkorderToSaleorder()
        CreateSalesorderToSchematic()   
