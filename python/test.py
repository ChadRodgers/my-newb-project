# load dependencies
from tkinter import *
from tkinter import ttk
from tkinter import messagebox
import sqlite3
from jetter_sql import *

class MyApp:
    def __init__(self, master):
        self.master = master
        self.frame1 = ttk.Frame(master)
        master.title('Jetter')
        menubar = Menu(master)
        filemenu = Menu(menubar, tearoff=0)
        filemenu.add_command(label="Edit Work Orders")
        filemenu.add_command(label="Edit Data", command=self.edit_data_window)
        filemenu.add_command(label="Exit", command=master.quit)
        menubar.add_cascade(label="Options", menu=filemenu)
        master.config(menu=menubar)
        self.frame1.grid()

    # GUI here
        #create work order label and entry field
        self.wo_label = Label(master, text="Work Order: ")
        self.wo_label.grid(row=2, column=1)
        self.workorder = Entry(font='Courier 12', width=15, relief=SUNKEN)
        self.workorder.grid(row=2, column=2, sticky='w')

        #create button to get the sales orders associated to the work order
        self.orders_button = Button(master, text="Get Sales Orders", command=self.get_orders)
        self.orders_button.grid(row=5, column=2, padx=10, sticky='w')

        #create text field for a message to the user
        self.note_text = Text(master, font='Courier 10 bold', fg='black', bg=self.frame1._root().cget('bg'),
            width=20, height=5, relief=FLAT, wrap=WORD)
        self.note_text.grid(row=2, column=2, sticky='e')
        note='''Please enter a work order to get the associated sale order information.'''
        self.note_text.insert(END, note)
        self.note_text.config(state=DISABLED)

        #create button to clear all the fields
        self.clear_button = Button(master, text="Clear All Fields", command=self.clear)
        self.clear_button.grid(row=5, column=2, padx=10, sticky='e')

        #create text field for returning data from db
        self.salesorder = Text(font='Courier 8', width=100, height=20, relief=SUNKEN)
        self.salesorder.grid(row=7, column=2, padx=10, pady=10)

        #create button to get override line values for schematics
        self.get_lines_button = Button(master, text="Get Schematics Line Values")
        self.get_lines_button.grid(row=8, column=2, padx=10, pady=10, sticky='e')

    
    # CREATE CHILD WINDOW FOR CREATE EDIT DATA
    def edit_data_window(self):
        child = Tk()
        child.title('Create / Update / Delete Sales Orders')
        child.configure(bg='gray')
        child.grid()

        ### EDIT SALESORDERS ###
        Label(child, bg='gray', text='Sales Order: ').grid(row=0, column=0, padx=5, pady=5, sticky='w')
        self.edit_salesorder = Entry(child, font='Courier 12', width=15, relief=SUNKEN)
        self.edit_salesorder.grid(row=0, column=1, sticky='w')
        Button(child, text="Search", command=self.get_sales_order).grid(row=0, column=2, padx=5, pady=5, sticky='w')
        
        self.edit_so_description = Text(child, font='Courier 8', width=25, height=4, relief=SUNKEN, wrap=WORD)
        self.edit_so_description.grid(row=1, column=1, pady=5)

        self.edit_so_note = Text(child, font='Courier 8', width=25, height=4, relief=SUNKEN, wrap=WORD)
        self.edit_so_note.grid(row=1, column=2, padx=2, pady=5)

        Button(child, text='Create/Update Sales Order Details', command=self.set_sales_order).grid(row=2, column=1, columnspan=2, pady=5)

        ### EDIT SCHEMATICS ###
        Label(child, bg='gray', text='Schematic: ').grid(row=3, column=0, padx=5, pady=25, sticky='w')
        self.edit_schematic = Entry(child, font='Courier 12', width=15, relief=SUNKEN)
        self.edit_schematic.grid(row=3, column=1, sticky='w')
        Button(child, text="Search", command=self.get_schematic).grid(row=3, column=2, padx=5, pady=25, sticky='w')
        
        self.edit_sch_description = Text(child, font='Courier 8', width=25, height=4, relief=SUNKEN, wrap=WORD)
        self.edit_sch_description.grid(row=4, column=1, pady=5)

        self.edit_sch_note = Text(child, font='Courier 8', width=25, height=4, relief=SUNKEN, wrap=WORD)
        self.edit_sch_note.grid(row=4, column=2, padx=2, pady=5)

        self.edit_sch_code = Text(child, font='Courier 8', width=25, height=4, relief=SUNKEN, wrap=WORD)
        self.edit_sch_code.grid(row=4, column=3, padx=2, pady=5)

        Button(child, text='Create/Update Schematic Details', command=self.set_schematic).grid(row=5, column=1, columnspan=2, pady=5)


########################### CHILD SALESORDER FUNCTIONALITY ###########################
    def get_sales_order(self):
        sales_order = self.edit_salesorder.get()
        self.clear_edit_salesorder()
        record = getRecord('salesorder', sales_order)
        if record:
            self.edit_so_description.insert(INSERT, record[0])
            self.edit_so_note.insert(INSERT, record[1])
        else:
            self.edit_so_description.insert(INSERT, 'No Data Found')
            self.edit_so_note.insert(INSERT, 'No Data Found')

    def set_sales_order(self):
        new_sales_order = self.edit_salesorder.get()
        new_description = self.edit_so_description.get("1.0", 'end-1c')
        new_note = self.edit_so_note.get("1.0", 'end-1c')
        Salesorder.setSalesorder(new_sales_order, new_description, new_note)

########################### CHILD SCHEMATIC FUNCTIONALITY ###########################
    def get_schematic(self):
        schematic = self.edit_schematic.get()
        self.clear_edit_schematic()
        record = getRecord('schematic', schematic)
        if record:
            self.edit_sch_description.insert(INSERT, record[0])
            self.edit_sch_note.insert(INSERT, record[1])
            self.edit_sch_code.insert(INSERT, record[2])
        else:
            self.edit_sch_description.insert(INSERT, 'No Data Found')
            self.edit_sch_note.insert(INSERT, 'No Data Found')
            self.edit_sch_code.insert(INSERT, 'No Data Found')

    def set_schematic(self):
        new_schematic = self.edit_schematic.get()
        new_sch_description = self.edit_sch_description.get("1.0", 'end-1c')
        new_sch_note = self.edit_sch_note.get("1.0", 'end-1c')
        new_sch_code = self.edit_sch_code.get("1.0", 'end-1c')
        Schematic.setSchematic(new_schematic, new_sch_description, new_sch_note, new_sch_code)

########################### MAIN PROGRAM FUNCTIONALITY ###########################
    def get_orders(self):
        wo = self.workorder.get()
 
        try:
            self.salesorder.delete(1.0, END)
            workorder_id = Workorder.getWorkOrderID(wo)

            if workorder_id:
                related_salesorders = getRelated('salesorder', 'workorder', workorder_id)
            
                #iterate through related salesorder records
                for record in related_salesorders:
                    so_id = record[0]
                    i = 1
                    while i < len(record)-2:
                        self.salesorder.insert(INSERT, record[i])
                        self.salesorder.insert(INSERT, '\t\t'*i)
                        i += 1
                    self.salesorder.insert(END, '\n')
                    
                    related_schematics = getRelated('schematic', 'salesorder', so_id)
               
                    #iterate through schematics
                    for schematic in related_schematics:
                        s = 1
                        self.salesorder.insert(INSERT, '  - ')
                        while s < len(schematic)-2:
                            self.salesorder.insert(INSERT, schematic[s])
                            self.salesorder.insert(INSERT, '\t\t')
                            s += 1
                        self.salesorder.insert(END, '\n')
                    self.salesorder.insert(END, '\n')

        # exception throw messagebox to user saying the workorder is not found in the DB            
        except:
            messagebox.showinfo("Oh no!",  'No work order found')
        

    # clear fields functions
    def clear(self):
        try:
            self.salesorder.delete(1.0, END)
            self.workorder.delete(0, END)
        except:
            pass

    def clear_edit_salesorder(self):
        try:
            self.edit_so_description.delete(1.0, END)
            self.edit_so_note.delete(1.0, END)
        except:
            pass

    def clear_edit_schematic(self):
        try:
            self.edit_sch_description.delete(1.0, END)
            self.edit_sch_code.delete(1.0, END)
            self.edit_sch_note.delete(1.0,END)
        except:
            pass
   

def main():
    root = Tk()
    MyApp(root)
    root.mainloop()


if __name__ == "__main__":
    main()