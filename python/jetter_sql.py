########### SQL CRUD FUNCTIONS TO GET, SET, DELETE DATA FROM DATABASE ###########

import sqlite3
from tkinter import messagebox

class Database:
    
    @staticmethod
    def openDatabase():
        try:
            conn = sqlite3.connect('jetter.db')
        except:
            messagebox.showinfo('Major Problem!',  'Unable to open the database.')
        return conn

############### WORK ORDERS CRUD ###############
class Workorder:
    def __init__(self, workorder):
        self.workorder = workorder    
        
    @staticmethod
    def getWorkOrderID(value):
        try:
            c = Database.openDatabase()
            id = c.execute(f'SELECT id from workorder WHERE ord = {value}').fetchone()[0]
            c.close()
            return id
        except:
            messagebox.showinfo('Oh no! ',  'No work order found')

    @staticmethod
    def setWorkorder(value):
        try:
            c = Database.openDatabase()
            c.execute('INSERT INTO workorder (ord) VALUES ({})'.format(value))
            c.close()
        except:
            messagebox.showinfo('Error inserting work order in to database')

############### SALES ORDERS CRUD ###############
class Salesorder:
    def __init__(self, salesorder, description, note):
        self.salesorder = salesorder
        self.description = description
        self.note = note

    @staticmethod
    def setSalesorder(salesorder, description, note):
        try:
            c = Database.openDatabase()
            c.execute(f"UPDATE salesorder SET desc='{description}', note='{note}' WHERE ord='{salesorder}'")
            c.execute('INSERT OR IGNORE INTO salesorder (ord, desc, note) VALUES (?, ?, ?)', 
                (salesorder, description, note))
            c.commit()
            c.close()
        except:
            messagebox.showinfo('Error inserting sales order in to database')

############### SCHEMATICS CRUD ###############
class Schematic:
    def __init__(self, schematic, description, s_type, note):
        self.schematic = schematic
        self.description = description
        self.s_type = s_type
        self.note = note

    @staticmethod
    def setSchematic(schematic, description, s_type, note):
        try:
            c = Database.openDatabase()
            c.execute(f"UPDATE schematic SET desc='{description}', type='{s_type}', note='{note}' WHERE item='{schematic}'")
            c.execute('INSERT OR IGNORE INTO schematic (item, desc, type, note) VALUES (?, ?, ?, ?)', 
                (schematic, description, s_type, note))
            c.commit()
            c.close()
        except:
            messagebox.showinfo('Error inserting schematic in to database')

############### COMMON GET RECORD SQL ###############
def getRecord(table, value):
    try:
        if table == 'schematic':
            column = 'item'
        else:
            column = 'ord'

        c = Database.openDatabase()
        record = c.execute(f"SELECT * FROM {table} WHERE {table}.{column} = '{value}'")
        for value in record:
            desc = value[2]
            note = value[3]
            if len(value) > 4:
                code = value[4]
                c.close()
                return desc, note, code
            else:
                c.close()
                return desc, note
    except:
        messagebox.showinfo(f'Unable to find {table}.{column} {value}.')

############### COMMON GET RELATED RECORDS SQL ###############
def getRelated(child_table, parent_table, value):
    try:
        if child_table == 'schematic':
            relate_table = 'so_to_sch'
            child_id = 'sch_id'
            parent_id = 'so_id'
        else:
            relate_table = 'wo_to_so'
            child_id = 'so_id'
            parent_id = 'wo_id'
            
        c = Database.openDatabase()
        related_records = c.execute(f'''SELECT {child_table}.*, {parent_table}.id, {parent_table}.ord
            FROM {child_table}
                JOIN
                    {relate_table} ON {child_table}.id = {relate_table}.{child_id}
                JOIN
                    {parent_table} ON {parent_table}.id = {relate_table}.{parent_id}
                WHERE
                    {parent_table}.id = {value}''').fetchall()
        c.close()
        return related_records
    except:
        messagebox.showinfo(f'Error getting related {child_table} records from {parent_table}')

############### COMMON RELATE ITEMS SQL ###############
def relateData(table, parent_id, child_id):
    try:
        c = Database.openDatabase()
        c.execute(f'INSERT INTO {table} ({parent_id}, {child_id}) VALUES ({parent_id}, {child_id})')
        c.close()
    except:
        messagebox.showinfo(f'Error relating data in {table} database table.')

############### COMMON DELETE RECORD SQL ###############
def delRecord(table, value):
    try:
        if table == 'schematic':
            column = 'item'
        else:
            column = 'ord'

        c = Database.openDatabase()
        c.execute(f'DELETE FROM {table} WHERE {column}={value}')
        c.close()
    except:
        messagebox.showinfo(f'Error deleting {value} record from {table} database table.')


if __name__ == "__main__":
    pass
